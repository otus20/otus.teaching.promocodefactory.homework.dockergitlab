FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
WORKDIR /src
COPY ./ ./
RUN dotnet restore "./src/Otus.Teaching.PromoCodeFactory.sln"
RUN dotnet build "./src/Otus.Teaching.PromoCodeFactory.sln" -c Release -o /app/build
RUN dotnet publish "./src/Otus.Teaching.PromoCodeFactory.WebHost/Otus.Teaching.PromoCodeFactory.WebHost.csproj" -c Release -o /app/publish

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS final
WORKDIR /app
ENV ASPNETCORE_URLS=http://*:5000
COPY --from=build /app/publish ./
CMD dotnet Otus.Teaching.PromoCodeFactory.WebHost.dll
