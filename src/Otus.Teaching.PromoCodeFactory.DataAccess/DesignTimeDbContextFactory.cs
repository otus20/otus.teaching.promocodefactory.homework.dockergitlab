﻿using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
   public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<DataContext>
   {
      public DataContext CreateDbContext(string[] args)
      {
         IConfiguration config = new ConfigurationBuilder()
            .SetBasePath(Path.Combine(Directory.GetCurrentDirectory(), "../Otus.Teaching.PromoCodeFactory.WebHost"))
            .AddJsonFile("appsettings.json")
            .Build();

         var connectionString = config.GetConnectionString("PromoCodeFactoryDb");

         var optionsBuilder = new DbContextOptionsBuilder<DataContext>();
         optionsBuilder.UseNpgsql(connectionString);

         return new DataContext(optionsBuilder.Options);
      }
   }
}
